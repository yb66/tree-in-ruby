require_relative "../lib/tree.rb"
require 'docopt'
require_relative "../lib/tree/options.rb"

require_relative "../lib/tree/version.rb"

script_name = Pathname(__FILE__).basename.to_s

if ENV["DEBUG"]
  warn "DEBUG MODE ON"
  require 'pry-byebug'
  require 'pry-state'
  binding.pry
end

USAGE = <<USAGE
tree - list contents of directories in a tree-like format.

Usage:
  #{script_name} <dir> [-a] [-d] [-f] [-L <level>] [-P <pattern>] [-I <pattern>] [--noreport] [--filelimit <n>] [--timefmt <fmt>] [-D] [-c] [-t] [-U] [-r] [--dirsfirst] [--sort=<name>]

Options:
  -h --help         Display help.
  --version         Show version and exit
  -a                All files are printed
  -d                Directories only
  -f                Prints the full path prefix for each file.
  -L <level>        Max display depth of the directory tree.
  -P <pattern>      List  only those files that match the wild-card pattern.
                    Note: you must use the -a option to also consider those files beginning with a dot `.' for matching.
                    Valid wildcard operators are:
                      `*' (any zero or more characters),
                      `?' (any  single  character),
                      `[...]'  (any single  character  listed  between  brackets  (optional - (dash) for character range may be used: ex: [A-Z]), and
                      `[^...]' (any single character not listed in brackets)
                      `|' separates alternate patterns.
  -I <pattern>      Do not list those files that match the wild-card pattern.
  --noreport        Omits printing of the file and directory report at the end of the tree listing.
  --filelimit <n>   Do not descend directories that contain more than n entries.
  --timefmt <fmt>   Prints (implies -D) and formats the date according to the format string which uses the strftime(3) syntax.
  -D                Print the date of the last modification time or if -c is used, the last status change time for the file listed.
  -t                Sort the output by last modification time instead of alphabetically.
  -c                Sort the output by last status change instead of alphabetically. 
                    Modifies the -D option (if used) to print the last status change instead of modification time.
  -U                Do not sort.  Lists files in directory order. Disables --dirsfirst.
  -r                Sort the output in reverse order.
                    This is a meta-sort that alter the above sorts.
                    This option is disabled when -U is used.
  --dirsfirst       List directories before files. This option is disabled when -U is used.
  --sort=<name>     Sort the output by name (as per ls): name (default), ctime (-c), mtime (-t), size.

USAGE


cleaned_options = Tree::Options.parse(USAGE)
begin
  runner = Tree::Runner.new cleaned_options.dir, options: cleaned_options
  runner.run
rescue => e
  warn e.message
  exit 1
end

exit 0
