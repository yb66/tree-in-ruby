require 'rspec'
require 'rspec/given'
require 'timecop'
require 'pathname'
require 'fileutils'

Spec_dir = Pathname(__dir__)
Fixtures_dir = Spec_dir.join("support/fixtures")

if ENV["DEBUG"]
  warn "DEBUG MODE ON"
  require 'pry-byebug'
  require 'pry-state'
  binding.pry
end

# code coverage
require 'simplecov'
SimpleCov.start do
  add_filter "/vendor/"
  add_filter "/vendor.noindex/"
  add_filter "/bin/"
  add_filter "/spec/"
  add_filter "/coverage/" # It used to do this for some reason, defensive of me.
end


Dir[ File.join( Spec_dir, "/support/**/*.rb")].each do |f|
  require f
end

TIME_NOW = Time.parse "2018-03-11T06:49:16+00:00"
WEEK_BEFORE = Time.parse "2018-03-04T12:45:19+00:00"

REAL_TIME_NOW = []
RSpec.configure do |config|
  config.expect_with(:rspec) { |c| c.syntax = [:should,:expect] }

  config.before(:each, :time_sensitive => true) do
    Timecop.freeze TIME_NOW
  end

  config.after(:each, :time_sensitive => true) do
    Timecop.return
  end

  # Files are set to the run time (real life, remember that?)
  # Directories to TIME_NOW
  tip1 = Fixtures_dir.join( "abc" )
  tip2 = Fixtures_dir.join( "mno" )
  branch1 = tip1.join( "def/ghi/jkl" )
  branch2 = tip2.join( "pqr/stu/vwx" )

  config.before(:suite) do
    tip1.rmtree if tip1.exist?
    tip2.rmtree if tip2.exist?

    # I'm not sure the second lazy is needed
    # but I like being lazy so why not?
    zipper = ("a".."z").cycle.lazy.zip((0..9).cycle.lazy)

    descender = ->(pn) {
      if pn.directory?
        #FileUtils.touch pn, mtime: WEEK_BEFORE
        pn.each_child do |child|
          descender.(child)
        end
      end
      _,m = zipper.next
      m.times do
        l,n = zipper.next
        dot = n.odd? ? "." : ""
        child = pn.join("#{dot}#{[l,n].join}")
        child.write l * n
        FileUtils.touch child, mtime: TIME_NOW
      end
    }


    [[tip1,branch1],[tip2,branch2]].each do |(tip,branch)|
      branch.mkpath
      descender.(Pathname(tip))
    end

    REAL_TIME_NOW << tip1.ctime # naughty way to set a "constant"
  end
  config.after(:suite) do
    Fixtures_dir.join( "abc").rmtree
    Fixtures_dir.join( "mno").rmtree
  end
end


include Tree