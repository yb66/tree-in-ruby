require 'spec_helper'
require_relative '../lib/tree.rb'

require 'stringio'

def capture_stdout(&blk)
  old = $stdout
  $stdout = fake = StringIO.new
  blk.call
  fake.string
ensure
  $stdout = old
end


describe Tree do
  Given(:runner) { runner = Tree::Runner.new base, options: {noreport: true}.merge(options) }

  shared_examples "Check the output" do |start=Fixtures_dir|
    When(:tree) {
      Dir.chdir start
      out = capture_stdout do
        runner.run
      end
      Dir.chdir __dir__
      # Remove the first line, it's the tree we're testing
      out.gsub(/[[:space:]]/,'')
    }
    Then { a_dir_structure == tree }
    And { runner.stats == stats }
  end

  context "No options" do
    Given(:a_dir_structure) {
      s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4

8 directories, 17 files
TREE
      s.gsub(/[[:space:]]/,'')
    }

    Given(:base) { "spec/support/fixtures" }
    # Remove the spaces to stop insanity
    Given(:runner) { runner = Tree::Runner.new base }
    Given(:stats) { {directories: 8, files: 17} }
    include_examples "Check the output", Spec_dir.parent
  end


  context "all files are printed option" do
    Given(:a_dir_structure) {
      s = <<TREE
spec/support/fixtures
├── abc
│   ├── .j9
│   ├── .l1
│   ├── .n3
│   ├── def
│   │   ├── .f5
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── .d9
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── .z5
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── .r7
        │   │   ├── .t9
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
      s.gsub(/[[:space:]]/,'')
    }
    Given(:base) { "spec/support/fixtures" }
    Given(:options){ {a: true} }
    Given(:stats) { {directories: 8, files: 27} }
    include_examples "Check the output", Spec_dir.parent
  end


  context "Dirs only" do
    Given(:a_dir_structure) {
      s = <<TREE
.
├── abc
│   └── def
│       └── ghi
│           └── jkl
└── mno
    └── pqr
        └── stu
            └── vwx
TREE
      s.gsub(/[[:space:]]/,'')
    }
    Given(:base){ "." }
    Given(:options) { {d: true} }
    Given(:stats) { {directories: 8, files: 0} }
    include_examples "Check the output"
  end


  context "Prints the full path prefix for each file" do
    context "dot" do
      Given(:a_dir_structure) {
        s = <<TREE
.
├── ./abc
│   ├── ./abc/def
│   │   ├── ./abc/def/e4
│   │   ├── ./abc/def/g6
│   │   └── ./abc/def/ghi
│   │       ├── ./abc/def/ghi/c2
│   │       └── ./abc/def/ghi/jkl
│   ├── ./abc/i8
│   ├── ./abc/k0
│   ├── ./abc/m2
│   └── ./abc/o4
└── ./mno
    ├── ./mno/c8
    ├── ./mno/e0
    ├── ./mno/g2
    ├── ./mno/i4
    └── ./mno/pqr
        ├── ./mno/pqr/a6
        ├── ./mno/pqr/stu
        │   ├── ./mno/pqr/stu/vwx
        │   │   ├── ./mno/pqr/stu/vwx/q6
        │   │   ├── ./mno/pqr/stu/vwx/s8
        │   │   └── ./mno/pqr/stu/vwx/u0
        │   └── ./mno/pqr/stu/w2
        └── ./mno/pqr/y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base){ "." }
      Given(:options) { {f: true} }
      Given(:stats) { {directories: 8, files: 17} }
      include_examples "Check the output"
    end

    context "spec/support/fixtures" do
      Given(:a_dir_structure) {
        s = <<TREE
support/fixtures
├── support/fixtures/abc
│   ├── support/fixtures/abc/def
│   │   ├── support/fixtures/abc/def/e4
│   │   ├── support/fixtures/abc/def/g6
│   │   └── support/fixtures/abc/def/ghi
│   │       ├── support/fixtures/abc/def/ghi/c2
│   │       └── support/fixtures/abc/def/ghi/jkl
│   ├── support/fixtures/abc/i8
│   ├── support/fixtures/abc/k0
│   ├── support/fixtures/abc/m2
│   └── support/fixtures/abc/o4
└── support/fixtures/mno
    ├── support/fixtures/mno/c8
    ├── support/fixtures/mno/e0
    ├── support/fixtures/mno/g2
    ├── support/fixtures/mno/i4
    └── support/fixtures/mno/pqr
        ├── support/fixtures/mno/pqr/a6
        ├── support/fixtures/mno/pqr/stu
        │   ├── support/fixtures/mno/pqr/stu/vwx
        │   │   ├── support/fixtures/mno/pqr/stu/vwx/q6
        │   │   ├── support/fixtures/mno/pqr/stu/vwx/s8
        │   │   └── support/fixtures/mno/pqr/stu/vwx/u0
        │   └── support/fixtures/mno/pqr/stu/w2
        └── support/fixtures/mno/pqr/y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base){ "support/fixtures" }
      Given(:options) { {f: true} }
      Given(:stats) { {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir
    end
  end


  context "Max display depth of the directory tree" do

    context "Level 0" do
      it "should complain" do
        expect {
          Tree::Runner.new ".", options: {L: 0}
        }.to raise_error(Tree::Error, "tree: Invalid level, must be greater than 0.")
      end
      #"tree: Invalid level, must be greater than 0."
    end
    context "Level 1" do
      context "dot" do
        Given(:a_dir_structure) {
          s = <<TREE
.
├── abc
└── mno
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:base){ "." }
        Given(:options) { {L: 1} }
        Given(:stats) { {directories: 2, files: 0} }
        include_examples "Check the output"
      end

      context "support/fixtures" do
        Given(:a_dir_structure) {
          s = <<TREE
support/fixtures
├── abc
└── mno

2 directories, 0 files
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:base){ "support/fixtures" }
        Given(:options){ {L: 1} }
        Given(:runner){ runner = Tree::Runner.new base, options: options }
        Given(:stats) { {directories: 2, files: 0} }
        include_examples "Check the output", Spec_dir
      end
    end
    context "Level 2" do
      context "." do
        Given(:a_dir_structure) {
          s = <<TREE
.
├── abc
│   ├── def
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:base){ "." }
        Given(:options) { {L: 2} }
        Given(:stats) { {directories: 4, files: 8} }
        include_examples "Check the output"
      end
      context "support/fixtures" do
        Given(:a_dir_structure) {
          s = <<TREE
support/fixtures
├── abc
│   ├── def
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:base){ "support/fixtures" }
        Given(:options) { {L: 2} }
        Given(:stats) { {directories: 4, files: 8} }
        include_examples "Check the output", Spec_dir
      end
    end
    context "Level 3" do
      context "dot" do
        Given(:a_dir_structure) {
          s = <<TREE
.
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        └── y4
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:base){ "." }
        Given(:options) { {L: 3} }
        Given(:stats) { {directories: 6, files: 12} }
        include_examples "Check the output"
      end
      context "support/fixtures" do
        Given(:a_dir_structure) {
          s = <<TREE
support/fixtures
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        └── y4
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:base){ "support/fixtures" }
        Given(:options) { {L: 3} }
        Given(:stats) { {directories: 6, files: 12} }
        include_examples "Check the output", Spec_dir
      end
    end
  end


  context "Given an invalid argument (a file)" do
    Given(:base){ Fixtures_dir.join("abc/i8") }
    it "should complain" do
      expect {
        runner = Tree::Runner.new base
        runner.run
      }.to raise_error(Tree::Error, "#{base} [error opening dir, it's a file]")
      #.and runner.stats == {directories: 0, files: 0}
    end
  end

  context "filelimit option" do
    context "--filelimit 3" do
      Given(:base) { "spec/support/fixtures" }
      context "And no further options" do
        Given(:a_dir_structure) {
          s = <<TREE
spec/support/fixtures
├── abc [5 entries exceeds filelimit, not opening dir]
└── mno [5 entries exceeds filelimit, not opening dir]
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:options){ {filelimit: 3} }
        Given(:stats) { {directories: 2, files: 0} }
        include_examples "Check the output", Spec_dir.parent
      end
      context "and -a (dot files too)" do
        Given(:a_dir_structure) {
          s = <<TREE
spec/support/fixtures
├── abc [8 entries exceeds filelimit, not opening dir]
└── mno [8 entries exceeds filelimit, not opening dir]
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:options) { {filelimit: 3, a: true} }
        Given(:stats){ {directories: 2, files: 0} }
        include_examples "Check the output", Spec_dir.parent
      end
    end
    context "--filelimit 5" do
      Given(:base) { "spec/support/fixtures" }
      context "And no further options" do
        Given(:a_dir_structure) {
          s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:options){ {filelimit: 5} }
        Given(:stats) { {directories: 8, files: 17} }
        include_examples "Check the output", Spec_dir.parent
      end
      context "and -a (dot files too)" do
        Given(:a_dir_structure) {
          s = <<TREE
spec/support/fixtures
├── abc [8 entries exceeds filelimit, not opening dir]
└── mno [8 entries exceeds filelimit, not opening dir]
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:options) { {filelimit: 5, a: true} }
        Given(:stats){ {directories: 2, files: 0} }
        include_examples "Check the output", Spec_dir.parent
      end
    end
    context "--filelimit 0" do
      Given(:base) { "spec/support/fixtures" }
      context "And no further options" do
        Given(:a_dir_structure) {
          s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:options){ {filelimit: 0} }
        Given(:stats) { {directories: 8, files: 17} }
        include_examples "Check the output", Spec_dir.parent
      end
      context "and -a (dot files too)" do
        Given(:a_dir_structure) {
          s = <<TREE
spec/support/fixtures
├── abc
│   ├── .j9
│   ├── .l1
│   ├── .n3
│   ├── def
│   │   ├── .f5
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── .d9
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── .z5
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── .r7
        │   │   ├── .t9
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
          s.gsub(/[[:space:]]/,'')
        }
        Given(:options) { {filelimit: 0, a: true} }
        Given(:stats){ {directories: 8, files: 27} }
        include_examples "Check the output", Spec_dir.parent
      end
    end
  end


  # There is no way to check the at_exit handler than I can find
  # using RSpec but it is possible to check it doesn't break anything
  # else.
  context "No report" do
    Given(:a_dir_structure) {
      s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
      s.gsub(/[[:space:]]/,'')
    }

    Given(:base) { "spec/support/fixtures" }
    Given(:options){ {noreport: true} }
    # Remove the spaces to stop insanity
    Given(:stats) { {directories: 8, files: 17} }
    include_examples "Check the output", Spec_dir.parent
  end


  context "dirsfirst" do
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── e4
│   │   └── g6
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── pqr
    │   ├── stu
    │   │   ├── vwx
    │   │   │   ├── q6
    │   │   │   ├── s8
    │   │   │   └── u0
    │   │   └── w2
    │   ├── a6
    │   └── y4
    ├── c8
    ├── e0
    ├── g2
    └── i4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {dirsfirst: true} }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "with -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── .f5
│   │   ├── e4
│   │   └── g6
│   ├── .j9
│   ├── .l1
│   ├── .n3
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── pqr
    │   ├── stu
    │   │   ├── vwx
    │   │   │   ├── .r7
    │   │   │   ├── .t9
    │   │   │   ├── q6
    │   │   │   ├── s8
    │   │   │   └── u0
    │   │   └── w2
    │   ├── .z5
    │   ├── a6
    │   └── y4
    ├── .d9
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    └── i4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {dirsfirst: true, a: true} }
      Given(:stats){ {directories: 8, files: 27} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "With --filelimit 5" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── e4
│   │   └── g6
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── pqr
    │   ├── stu
    │   │   ├── vwx
    │   │   │   ├── q6
    │   │   │   ├── s8
    │   │   │   └── u0
    │   │   └── w2
    │   ├── a6
    │   └── y4
    ├── c8
    ├── e0
    ├── g2
    └── i4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {dirsfirst: true, filelimit: 5 } }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "With --filelimit 5 and -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc [8 entries exceeds filelimit, not opening dir]
└── mno [8 entries exceeds filelimit, not opening dir]
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {dirsfirst: true, filelimit: 4, a: true } }
      Given(:stats){ {directories: 2, files: 0} }
      include_examples "Check the output", Spec_dir.parent
    end
  end


  context "Unsorted" do
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── g6
│   │   └── e4
│   ├── i8
│   ├── o4
│   ├── k0
│   └── m2
└── mno
    ├── pqr
    │   ├── y4
    │   ├── a6
    │   └── stu
    │       ├── w2
    │       └── vwx
    │           ├── u0
    │           ├── q6
    │           └── s8
    ├── c8
    ├── g2
    ├── i4
    └── e0
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {U: true} }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and --dirsfirst" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── g6
│   │   └── e4
│   ├── i8
│   ├── o4
│   ├── k0
│   └── m2
└── mno
    ├── pqr
    │   ├── y4
    │   ├── a6
    │   └── stu
    │       ├── w2
    │       └── vwx
    │           ├── u0
    │           ├── q6
    │           └── s8
    ├── c8
    ├── g2
    ├── i4
    └── e0
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {U: true, dirsfirst: true} }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
  end


  context "Sort by -t modification time" do
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── i8
│   ├── k0
│   ├── m2
│   ├── o4
│   └── def
│       ├── e4
│       ├── g6
│       └── ghi
│           ├── c2
│           └── jkl
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── y4
        └── stu
            ├── w2
            └── vwx
                ├── q6
                ├── s8
                └── u0
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {t: true} }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and --dirsfirst -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── .f5
│   │   ├── e4
│   │   └── g6
│   ├── .j9
│   ├── .l1
│   ├── .n3
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── pqr
    │   ├── stu
    │   │   ├── vwx
    │   │   │   ├── .r7
    │   │   │   ├── .t9
    │   │   │   ├── q6
    │   │   │   ├── s8
    │   │   │   └── u0
    │   │   └── w2
    │   ├── .z5
    │   ├── a6
    │   └── y4
    ├── .d9
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    └── i4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {t: true, a: true, dirsfirst: true} }
      Given(:stats){ {directories: 8, files: 27} }
      include_examples "Check the output", Spec_dir.parent
    end
  end


  context "Sort by -c changed time" do
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {c: true} }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── .j9
│   ├── .l1
│   ├── .n3
│   ├── def
│   │   ├── .f5
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── .d9
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── .z5
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── .r7
        │   │   ├── .t9
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {c: true, a: true} }
      Given(:stats){ {directories: 8, files: 27} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -a --dirsfirst" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── .f5
│   │   ├── e4
│   │   └── g6
│   ├── .j9
│   ├── .l1
│   ├── .n3
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── pqr
    │   ├── stu
    │   │   ├── vwx
    │   │   │   ├── .r7
    │   │   │   ├── .t9
    │   │   │   ├── q6
    │   │   │   ├── s8
    │   │   │   └── u0
    │   │   └── w2
    │   ├── .z5
    │   ├── a6
    │   └── y4
    ├── .d9
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    └── i4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {c: true, a: true, dirsfirst: true} }
      Given(:stats){ {directories: 8, files: 27} }
      include_examples "Check the output", Spec_dir.parent
    end
  end



  context "Sort by -r reverse sort" do
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── mno
│   ├── pqr
│   │   ├── y4
│   │   ├── stu
│   │   │   ├── w2
│   │   │   └── vwx
│   │   │       ├── u0
│   │   │       ├── s8
│   │   │       └── q6
│   │   └── a6
│   ├── i4
│   ├── g2
│   ├── e0
│   └── c8
└── abc
    ├── o4
    ├── m2
    ├── k0
    ├── i8
    └── def
        ├── ghi
        │   ├── jkl
        │   └── c2
        ├── g6
        └── e4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {r: true} }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── mno
│   ├── pqr
│   │   ├── y4
│   │   ├── stu
│   │   │   ├── w2
│   │   │   └── vwx
│   │   │       ├── u0
│   │   │       ├── s8
│   │   │       ├── q6
│   │   │       ├── .t9
│   │   │       └── .r7
│   │   ├── a6
│   │   └── .z5
│   ├── i4
│   ├── g2
│   ├── e0
│   ├── c8
│   ├── .h3
│   ├── .f1
│   └── .d9
└── abc
    ├── o4
    ├── m2
    ├── k0
    ├── i8
    ├── def
    │   ├── ghi
    │   │   ├── jkl
    │   │   └── c2
    │   ├── g6
    │   ├── e4
    │   └── .f5
    ├── .n3
    ├── .l1
    └── .j9
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {r: true, a: true} }
      Given(:stats){ {directories: 8, files: 27} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -a --dirsfirst" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── mno
│   ├── pqr
│   │   ├── stu
│   │   │   ├── vwx
│   │   │   │   ├── u0
│   │   │   │   ├── s8
│   │   │   │   ├── q6
│   │   │   │   ├── .t9
│   │   │   │   └── .r7
│   │   │   └── w2
│   │   ├── y4
│   │   ├── a6
│   │   └── .z5
│   ├── i4
│   ├── g2
│   ├── e0
│   ├── c8
│   ├── .h3
│   ├── .f1
│   └── .d9
└── abc
    ├── def
    │   ├── ghi
    │   │   ├── jkl
    │   │   └── c2
    │   ├── g6
    │   ├── e4
    │   └── .f5
    ├── o4
    ├── m2
    ├── k0
    ├── i8
    ├── .n3
    ├── .l1
    └── .j9
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {r: true, a: true, dirsfirst: true} }
      Given(:stats){ {directories: 8, files: 27} }
      include_examples "Check the output", Spec_dir.parent
    end
  end
  context "--sort=size" do
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── ghi
│   │   │   ├── jkl
│   │   │   └── c2
│   │   ├── g6
│   │   └── e4
│   ├── i8
│   ├── o4
│   ├── m2
│   └── k0
└── mno
    ├── pqr
    │   ├── stu
    │   │   ├── vwx
    │   │   │   ├── s8
    │   │   │   ├── q6
    │   │   │   └── u0
    │   │   └── w2
    │   ├── a6
    │   └── y4
    ├── c8
    ├── i4
    ├── g2
    └── e0
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:base) { "spec/support/fixtures" }
      Given(:options) { {sort: "size" } }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
  end

  context "-D" do
    Given(:frmt) { "%b %e %H:%M" }
    Given(:now) { REAL_TIME_NOW.first.strftime frmt }
    Given(:base) { "spec/support/fixtures" }
    context "No other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── [#{now}]  abc
│   ├── [#{now}]  def
│   │   ├── [Mar 11  2018]  e4
│   │   ├── [Mar 11  2018]  g6
│   │   └── [#{now}]  ghi
│   │       ├── [Mar 11  2018]  c2
│   │       └── [#{now}]  jkl
│   ├── [Mar 11  2018]  i8
│   ├── [Mar 11  2018]  k0
│   ├── [Mar 11  2018]  m2
│   └── [Mar 11  2018]  o4
└── [#{now}]  mno
    ├── [Mar 11  2018]  c8
    ├── [Mar 11  2018]  e0
    ├── [Mar 11  2018]  g2
    ├── [Mar 11  2018]  i4
    └── [#{now}]  pqr
        ├── [Mar 11  2018]  a6
        ├── [#{now}]  stu
        │   ├── [#{now}]  vwx
        │   │   ├── [Mar 11  2018]  q6
        │   │   ├── [Mar 11  2018]  s8
        │   │   └── [Mar 11  2018]  u0
        │   └── [Mar 11  2018]  w2
        └── [Mar 11  2018]  y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {D: true } }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -c" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── [#{now}]  abc
│   ├── [#{now}]  def
│   │   ├── [#{now}]  e4
│   │   ├── [#{now}]  g6
│   │   └── [#{now}]  ghi
│   │       ├── [#{now}]  c2
│   │       └── [#{now}]  jkl
│   ├── [#{now}]  i8
│   ├── [#{now}]  k0
│   ├── [#{now}]  m2
│   └── [#{now}]  o4
└── [#{now}]  mno
    ├── [#{now}]  c8
    ├── [#{now}]  e0
    ├── [#{now}]  g2
    ├── [#{now}]  i4
    └── [#{now}]  pqr
        ├── [#{now}]  a6
        ├── [#{now}]  stu
        │   ├── [#{now}]  vwx
        │   │   ├── [#{now}]  q6
        │   │   ├── [#{now}]  s8
        │   │   └── [#{now}]  u0
        │   └── [#{now}]  w2
        └── [#{now}]  y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {D: true, c: true } }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "--timefmt %B %d %Y" do
      Given(:frmt) { "%B %d %Y" }
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── [#{now}]  abc
│   ├── [#{now}]  def
│   │   ├── [March 11 2018]  e4
│   │   ├── [March 11 2018]  g6
│   │   └── [#{now}]  ghi
│   │       ├── [March 11 2018]  c2
│   │       └── [#{now}]  jkl
│   ├── [March 11 2018]  i8
│   ├── [March 11 2018]  k0
│   ├── [March 11 2018]  m2
│   └── [March 11 2018]  o4
└── [#{now}]  mno
    ├── [March 11 2018]  c8
    ├── [March 11 2018]  e0
    ├── [March 11 2018]  g2
    ├── [March 11 2018]  i4
    └── [#{now}]  pqr
        ├── [March 11 2018]  a6
        ├── [#{now}]  stu
        │   ├── [#{now}]  vwx
        │   │   ├── [March 11 2018]  q6
        │   │   ├── [March 11 2018]  s8
        │   │   └── [March 11 2018]  u0
        │   └── [March 11 2018]  w2
        └── [March 11 2018]  y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {D: true, timefmt: frmt } }
      Given(:stats){ {directories: 8, files: 17} }
      include_examples "Check the output", Spec_dir.parent
    end
  end



  context "-P <pattern>" do
    context "and no other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   └── def
│       ├── e4
│       └── ghi
│           └── jkl
└── mno
    ├── e0
    └── pqr
        └── stu
            └── vwx
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {P: 'e*' }}
      Given(:base) { "spec/support/fixtures" }
      # Remove the spaces to stop insanity
      Given(:stats) { {directories: 8, files: 2} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── .j9
│   └── def
│       └── ghi
│           └── jkl
└── mno
    ├── .d9
    └── pqr
        └── stu
            └── vwx
                └── .t9
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {P: '*9', a: true } }
      Given(:base) { "spec/support/fixtures" }
      # Remove the spaces to stop insanity
      Given(:stats) { {directories: 8, files: 3} }
      include_examples "Check the output", Spec_dir.parent
    end
  end


  context "-I <pattern>" do
    context "and no other options" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── def
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── c8
    ├── g2
    ├── i4
    └── pqr
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {I: 'e*' }}
      Given(:base) { "spec/support/fixtures" }
      # Remove the spaces to stop insanity
      Given(:stats) { {directories: 8, files: 15} }
      include_examples "Check the output", Spec_dir.parent
    end
    context "and -a" do
      Given(:a_dir_structure) {
        s = <<TREE
spec/support/fixtures
├── abc
│   ├── .l1
│   ├── .n3
│   ├── def
│   │   ├── .f5
│   │   ├── e4
│   │   ├── g6
│   │   └── ghi
│   │       ├── c2
│   │       └── jkl
│   ├── i8
│   ├── k0
│   ├── m2
│   └── o4
└── mno
    ├── .f1
    ├── .h3
    ├── c8
    ├── e0
    ├── g2
    ├── i4
    └── pqr
        ├── .z5
        ├── a6
        ├── stu
        │   ├── vwx
        │   │   ├── .r7
        │   │   ├── q6
        │   │   ├── s8
        │   │   └── u0
        │   └── w2
        └── y4
TREE
        s.gsub(/[[:space:]]/,'')
      }
      Given(:options) { {I: '*9', a: true } }
      Given(:base) { "spec/support/fixtures" }
      # Remove the spaces to stop insanity
      Given(:stats) { {directories: 8, files: 24} }
      include_examples "Check the output", Spec_dir.parent
    end
  end
end
