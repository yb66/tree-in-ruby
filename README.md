# Tree (in Ruby)

* [Homepage](https://rubygems.org/gems/tree)
* [Documentation](http://rubydoc.info/gems/tree/frames)
* [Email](mailto:helpful-iain at theprintedbirn.com)

[![Build Status](https://secure.travis-ci.org//tree.svg?branch=master)](https://travis-ci.org//tree)

## Description

An implementation of the classic Unix `tree` command in Ruby, because I always miss it when it's not there.

## Examples

  Once installed:

    tree .

  If you were in this repo and had run the specs:

    tree spec/support/fixtures

  Output:

    spec/support/fixtures
    ├── abc
    │   ├── d1
    │   ├── d2
    │   ├── d3
    │   ├── def
    │   │   ├── a5
    │   │   ├── a6
    │   │   ├── ghi
    │   │   │   └── jkl
    │   │   ├── l10
    │   │   ├── l8
    │   │   └── l9
    │   ├── h5
    │   ├── h6
    │   └── h7
    └── pqr
        └── stu
            └── ghi
                └── jkl



## Requirements

## Install

    git clone https://gitlab.com/yb66/tree-in-ruby
    cd tree-in-ruby
    bundle install --binstubs --path=vendor.noindex
    # Now you can run it!
    bundle exec ruby tree spec/support/fixtures

## Copyright

Copyright (c) 2019 iain barnett

See {file:LICENCE.txt} for details.
