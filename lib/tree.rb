require 'pathname'
require 'date'
require 'mustermann/shell'

# An implementation of the classic Unix `tree` command in Ruby,
# because I always miss it when it's not there.
module Tree

  # Symbol to use when there are more children to come.
  T = " ├──".freeze

  # Symol to use for last child.
  L = " └──".freeze

  # Symbol to use for a child while another branch is being displayed.
  I = " │  ".freeze

  # Space.
  N = "    ".freeze


  # Namspaced error.
  class Error < StandardError; end


  # The class that does most of the work.
  # @example
  #   cleaned_options = Tree::Options.parse(USAGE)
  #   begin
  #     runner = Tree::Runner.new cleaned_options.dir, options: cleaned_options
  #     runner.run
  #   rescue => e
  #     warn e.message
  #     exit 1
  #   end
  class Runner


    SORT_BY_MTIME = proc {|a,b|
      # can't trust `mtime`, sometimes it will produce 1 when
      # they are the same. Use `to_i`
      if (res = a.mtime.to_i <=> b.mtime.to_i).zero?
        a <=> b
      else
        res
      end
    }


    SORT_BY_CTIME = proc {|a,b|
      # I won't trust `ctime` because I can't trust `mtime`. Use `to_i`
      if (res = a.ctime.to_i <=> b.ctime.to_i).zero?
        a <=> b
      else
        res
      end
    }


    SORT_BY_SIZE = proc {|a,b|
      b.size <=> a.size
    }

    # @param base [#to_s] The starting directory.
    # @param options [#each_pair] Options via a hash like object.
    def initialize base, options: {}
      @base = base.to_s
      @base_pn = Pathname(@base) # memoize
      @options = options
      if @options[:filelimit] and @options[:filelimit].zero?
        @options.delete :filelimit
      end
      @stats = {directories: 0, files: 0}
      fail Error, "tree: Invalid level, must be greater than 0." if @options[:L] and @options[:L] <= 0
      @options.delete :dirsfirst if @options[:U]
      @today = Time.now.to_date
    end


    attr_accessor :stats


    TODAY_FORMAT = "%b %e %H:%M"
    OLDER_FORMAT = "%b %e  %Y"

    # @param pn [#basename] Path.
    # @param prefix [#join] Current prefix for this path.
    def render! pn, prefix: [], suffix: nil
      _prefix = prefix.dup
      if @options[:f]
        name =  @base_pn.join(pn.relative_path_from(@base_pn))
        name = "./#{name}" if @base == "." # This is to match unix-tree's output
      else
        name = pn.basename.to_s
      end
      if @options[:D]
        letter = @options[:c] ? "c" : "m"
        time = pn.send("#{letter}time")
        frmt = if @options[:timefmt]
          @options[:timefmt]
        else
          time.to_date == @today ? TODAY_FORMAT : OLDER_FORMAT
        end
        _prefix << " [#{time.strftime(frmt)}] "
      end
      sentence = "#{_prefix.join} #{name}"[1..-1]
      if suffix
        sentence << " #{suffix}"
      end
      puts sentence 
    end


    # @param pn [#basename] Path.
    # @param depth [Integer] Depth level for this path.
    # @param prefix [#join] Current prefix for this path.
    def _run pn, depth: 0, prefix: [], render: true
      return if @options[:L] and depth > @options[:L]
      if pn.directory?
        run_children = true
        children = @options[:a] ?
          pn.children :
          pn.children.reject{|child| child.basename.to_s.start_with? "." }

        if @options[:d]
          children.reject!{|child| child.file? }
        end
        if @options[:filelimit]
          if children.size > @options[:filelimit]
            suffix = " [#{children.size} entries exceeds filelimit, not opening dir]"
            run_children = false
          end
        end
        if @options[:P]
          pattern = Mustermann.new( @options[:P].taint, # Just in case.
                                    type: :shell )
          children.keep_if{|child| child.directory? or pattern === child.basename.to_s }
        end
        if @options[:I]
          pattern = Mustermann.new( @options[:I].taint, # Just in case.
                                    type: :shell )
          children.delete_if{|child| !child.directory? and pattern === child.basename.to_s }
        end
        unless @options[:U]
          if @options[:dirsfirst]
            dirs,files = children.partition{|child| child.directory? }
            dirs,files =
              if @options[:t]
                [dirs.sort(&SORT_BY_MTIME), files.sort(&SORT_BY_MTIME)]
              elsif @options[:c]
                [dirs.sort(&SORT_BY_CTIME), files.sort(&SORT_BY_CTIME)]
              else
                [dirs.sort, files.sort]
              end
            children.replace @options[:r] ?
              dirs.reverse! + files.reverse! :
              dirs + files
          else
            if @options[:t] or (@options[:sort] and @options[:sort] == "mtime")
              children.replace children.sort(&SORT_BY_MTIME)
            elsif @options[:c] or (@options[:sort] and @options[:sort] == "ctime")
              children.replace children.sort(&SORT_BY_CTIME)
            elsif @options[:sort] and @options[:sort] == "size"
              children.replace children.sort(&SORT_BY_SIZE)
            else
              children.sort!
            end
            if @options[:r]
              children.reverse!
            end
          end
        end
      end
      if render
        self.render! pn, prefix: prefix, suffix: suffix
        add_stat pn
      end
      if children and run_children
        if prefix.last == L
          prefix.pop
          prefix << N
        elsif prefix.last == T
          prefix.pop
          prefix << I
        end

        children.each_with_index do |child,index|
          suffix = if (index + 1) < children.size
            [T]
          else
            [L]
          end
          _run child, depth: depth + 1, prefix: prefix + suffix
        end
      end
    end


    # @param pn [#basename] Path.
    def add_stat pn
      if pn.directory?
        @stats[:directories] += 1
      else
        @stats[:files] += 1
      end
    end


    # Start walking the directory tree.
    def run
      fail Error, "#{@base} [error opening dir, it's a file]" if @base_pn.file?
      puts @base
      _run @base_pn, render: false
      unless @options[:noreport]
        puts "\n#{@stats[:directories]} directories, #{@stats[:files]} files"
      end
    end
  end

end
