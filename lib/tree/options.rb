require 'docopt'


module Tree

  # A helper class to parse and clean up Docopt options
  # @see https://github.com/docopt/docopt
  class Options
    include Enumerable


    # @param opts [#each_pair]
    def initialize opts
      @original = opts.dup
      @cleaned = self.class.clean @original
      pp @cleaned if @cleaned["dry-run"] and @cleaned["verbose"]
      self.class.add_methods @cleaned
    end


    # If you wish to delete it.
    def delete key
      @cleaned.delete key
    end


    # Iterate through the cleaned keys and values.
    def each
      @cleaned.each do |k,v|
        yield k, v
      end
    end


    # Retrieve a value via its key.
    # @param key [#to_sym]
    def []( key )
      @cleaned[key.to_sym]
    end


    # Is this key in the cleanded options?
    # @param key [#to_sym]
    # @return [TrueClass]
    def has_key? key
      @cleaned.has_key? key.to_sym
    end


    # Merge with the cleaned options.
    # @param opts [Hash]
    # @return [self]
    def merge! **opts
      @cleaned.merge! opts
      self.class.add_methods opts
      self
    end


    # The original hash provided by Docopt
    # @return [Hash]
    def docopt_original
      @original
    end


    class << self
      # Parse the usage instructions. May call `exit`.
      # @param usage [String] The usage instructions.
      # @return [Hash]
      def parse usage
        new Docopt::docopt(usage, help: true, version: VERSION)
      rescue Docopt::Exit => e
        puts e.message
        exit 0
      end


      # Typically, Docopt's hash keys are similar to the options provided
      # e.g. `{"--long-opt" => "6", <arg> => "file/path"}`
      # This cleans up the keys, e.g. `{:long_opt => "6", :arg => "file/path"}`
      # @param options [Hash]
      # @return [Hash]
      def clean options
        Hash[
          options.map{|(k,v)|
            key = k.sub( /^\-\-?/, "" )
                   .gsub(/\-/, "_")
                   .gsub(/<|>/, "")
                   .to_sym
            val =
              if v =~ /\A\d+(?:\.\d+)*\z/
                if v.include? "."
                  v.to_f
                else
                  v.to_i
                end
              else
                v
              end
            [ key, val ]
          }.compact
        ]
      end


      # Adds a method for each key in the cleaned options.
      # @param cleaned [Symbol]
      # TODO respond_to?
      def add_methods cleaned
        cleaned.each do |key,value|
          define_method key do
            value
          end
        end
        cleaned
      end

    end
  end

end