module Tree

  # Library version
  # @ see https://semver.org/spec/v2.0.0.html
  VERSION = "0.7.0"
end
