# Change log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

### Added


### 0.7.0 / 6th of February 2019

### Changed

- Don't use `at_exit` to output the report.
- Better specs and reverted to generation of the directory tree it relies on, but set up much better this time.

### Added

- Option `-D`, print the date of the last modification time.
- Option `--timefmt`, supply a time format.
- Option `-P <pattern>` matching.
- Option `-I <pattern>` exclusion


### 0.6.0 / 4th of February 2019

### Changed

- No longer generating the test files, it was finickity.

### Added

- Option `-U`, unsorted.
- Option `--t`, sort by file modification time.
- Option `--c`, sort by file changed time.
- Option `-r`, reverse sort.
- Option `--sort=name`, sort by name.


### 0.5.0 / 4th of February 2019

### Changed

- Fixed bug with the Options cleaning.
- Bit of refactoring to (start to) try and cut down on similar and expensive calls in too many and different places.
- Sorting should be faster by a magnitude when it's done.

### Added

- Option `--dirsfirst`, list directories before files.

### 0.4.0 / 4th of February 2019

### Changed

- Added noreport option to most of the specs to stop the report being printed at the end of them.

### Added

- Option `--noreport`, omits printing of the file and directory report at the end of the tree listing.


### 0.3.0 / 4th of February 2019

### Changed

- Improved the specs by DRYing them.
- The Options class will automagically convert integer and float values.

### Added

- Option `--filelimit <n>`, do not descend directories that contain more than <n> entries.


### 0.2.0 / 3rd of February 2019

### Changed

- Options is indifferent to strings and symbols when asked (but always uses symbols).
- Patched some possible bugs before they occur.

### Added

- 100% of code is documented with Yardoc.
- Statistics are output at the end of a run, e.g. 8 directories, 11 files
- Error message when not passed a directory to traverse.


### 0.1.0 / 3rd of February 2019

### Changed

- Fixed some of the README.
- Changed the API slightly, runner doesn't execute within the options block (that was weird).

### Added

- Option `-L level`, max display depth of the directory tree.


### 0.0.2 / 2nd of February 2019

### Changed

- Fixed some of the output to make it exactly like unix-tree.

### Added

- Option `-a`, all files are printed.
- Option `-d`, only dirs are printed.
- Option `-f`, Prints the full path prefix for each file (if given a relative path it's relative).
- Using docopt for options.


### 0.0.1 / 1st of February 2019

### Added

- Basic working tree command
- Set up of project
